import React from 'react';
import './Footer.css';

class Footer extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            
            <section>
                <footer className="footer"> 
                    <div className= "container" id="text-footer" >
                        <a  href="#about">Nazwa firmy - wszelkie prawa zastrzeżone, 2019</a>
                        <div className="footer-right">
                            <span> <i id="Insta-icon" className=" fab fa-instagram fa-2x"></i></span>
                            <span> <i id="fb-icon" className="fab fa-facebook-square fa-2x"></i></span>       
                        </div>         
                    </div>
                </footer>
            </section>
        )  
    }
}
export default Footer;  