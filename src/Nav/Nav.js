import React from 'react';
import './Nav.css';

class Nav extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <nav> 
                <div className="nav container">
                    <a  className="nav-container"id="nav-logo" href="#home">Nazwa firmy</a>
                    <label for="toggle">&#9776;</label>
                    <input type="checkbox" id="toggle"/>
                        <div className="menu container">
                            <a href="#about">O nas</a>
                            <a href="#offer">Oferta</a>
                            <a href="#"><span type="button" disabled>Kontakt</span></a>
                        </div>
                </div>
            </nav>
        )  
    }
}
export default Nav;  