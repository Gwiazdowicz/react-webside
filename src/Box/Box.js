import React from 'react';
import './Box.css';

class Box extends React.Component{
    constructor(props){
        super(props);

    }
    
    isNewText(){
        if(this.props.box.isNew)
        return <span>(nowość)</span>
    }
    isNewDot(){
        if(this.props.box.isNew)
        return <div className="box-new"></div> 
    }

    render(){
        return (<div>
                <div className="box-content">{this.props.box.name}{this.isNewText()}  </div>
                 {this.isNewDot()}           
                </div>     
        )  
    }
}
export default Box;  