import React from 'react';
import logo from './logo.svg';
import './App.css';
import Nav from './Nav/Nav';
import Home from './Home/Home';
import About from './About/About';
import Offer from './Offer/Offer';
import Footer from './Footer/Footer';



function App() {
  return (
   
    <div className="App">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css" integrity="sha384-i1LQnF23gykqWXg6jxC2ZbCbUMxyw5gLZY6UiUS98LYV5unm8GWmfkIS6jqJfb4E" crossorigin="anonymous"></link>
       <Nav></Nav>
       <Home></Home>
       <About></About>
       <Offer></Offer>
       <Footer></Footer>
      
      
    
    </div>
  );
}

export default App;
