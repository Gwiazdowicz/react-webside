import React from 'react';
import './About.css';
import Employee from '../Employee/Employee';
import emp1 from '../img/One.jpg';
import emp2 from '../img/Two.jpg';
import emp3 from '../img/Three.jpg';

class About extends React.Component{
    constructor(props){
        super(props);
        console.log(props);

        this.employees = [
            {
            name: "Jan Nowkowski",
            pic: emp1,
            },
        {
            name:"Anna Wawrzyniak",
            pic: emp2,
        },
        {
            name:"Janusz Król",
            pic: emp3, 
        },
        {
            name:"Joanna Poznański",
            pic: emp2,
        },
        ]
    }


    render(){
        return (
            <section id="about">
            <div className="container about-container">
                <div className="header-info">
                    <h1 id="about-head-text">Nasi specjaliści</h1>
                </div> 
                   {this.employees.map((emp, i) => {
                    return <Employee employee= {emp} key={i}></Employee>
                   })}      
            </div>
        </section>
        )  
    }
}
export default About;  