import React from 'react';
import './Employee.css';




class Employee extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
                <div className="person">
                  {/*  <div className="avatar"></div> */}
                  <img className="avatar" src={this.props.employee.pic} alt="Employee picture"></img>
                    <div><h3>{this.props.employee.name}</h3>
                        <p>Jest dostępnych wiele różnych wersji Lorem Ipsum, ale większość zmieniła 
                            się pod wpływem dodanego humoru czy przypadkowych słów, które nawet w 
                            najmniejszym stopniu nie przypominają istniejących. Jeśli masz zamiar użyć
                            fragmentu Lorem Ipsum, lepiej mieć pewność, że nie ma niczego „dziwnego” w środku tekstu.</p>
                    </div>
                </div>
        )  
    }
}
export default Employee;  