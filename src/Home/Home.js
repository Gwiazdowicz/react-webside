import React from 'react';
import './Home.css';

class Home extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <section id="home" className ="home">
                <div className ="home-shadow" >
                    <div className="container home-container">
                        <div className="home-text"> 
                            <h1>Nasza firma oferuje najwyższej jakości produkty</h1>
                            <h2>Nie wierz nam na słowo - sprawdź</h2>
                            <a className="btn"  href="#offer">Oferta</a>
                        </div>  
                    </div>       
                </div> 
            </section>
        )  
    }
}
export default Home;  